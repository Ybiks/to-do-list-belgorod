const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

require('babel-core/register');
/*['.css', '.less', '.sass', '.ttf', '.woff', '.woff2']
	.forEach((ext) => require.extensions[ext] = () => {});*/
require('babel-polyfill');


if(process.env.NODE_ENV === 'production'){
	if(cluster.isMaster) {
		console.log(`Master ${process.pid} is running`);

		for(let i = 0; i < numCPUs; i++){
			cluster.fork();
		}
	}else{
		require('./server/buildServer')
	}
}else if(process.env.NODE_ENV === "development"){
	require('./server/devServer');
}else{
	console.log(`Not strategy for ${process.env.NODE_ENV}`);
}