import async from "async";
import crypto from "crypto";
import randToken from "rand-token";
import mongoose from "../libs/connectMongoose";

const SchemaPattern = mongoose.Schema;

const Schema = new SchemaPattern({
	givenName: {
		type: String,
		required: true
	},
	familyName: {
		type: String,
		required: true
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	salt: {
		type: String,
		required: true
	},
	hashedPassword: {
		type: String,
		required: true
	},
	groups: {
		type: Array,
		default: [
			{
				id: randToken.generate(16),
				color: "#673ab7",
				title: "Личное",
				tasks: []
			},
			{
				id: randToken.generate(16),
				color: "#2196f3",
				title: "Покупки",
				tasks: []
			},
			{
				id: randToken.generate(16),
				color: "#ff9800",
				title: "Работа",
				tasks: []
			},
			{
				id: randToken.generate(16),
				color: "#ffeb3b",
				title: "Задания",
				tasks: []
			}
		]
	}
});

Schema.virtual("password")
	.set(function(password) {
		this._plainPassword = password;
		this.salt = Math.random() + "";
		this.hashedPassword = this.encryptPassword(password);
	})
	.get(function() {
		return this._plainPassword;
	});

Schema.methods.encryptPassword = function(password) {
	return crypto
		.createHmac("sha1", this.salt)
		.update(password)
		.digest("hex");
};

Schema.methods.checkPassword = function(password) {
	return this.encryptPassword(password) === this.hashedPassword;
};

Schema.statics.registration = function(
	{ givenName, familyName, email, password },
	callback
) {
	let User = this;

	async.waterfall(
		[
			callback => User.findOne({ email }, callback),

			(user, callback) => {
				if (user)
					return callback(true, false, {
						status: 406,
						message:
							"Указанный вами почтовый ящик, уже зарегистрирован"
					});

				let model = new User({
					givenName,
					familyName,
					email,
					password
				});

				model.save((err, user) => {
					if (!err) return callback(null, user); //Возвращаем пользователя

					//log
					callback(true, false, {
						status: 500,
						message: "Ошибка регистрации. Попробуйте позже"
					});
				});
			}
		],
		callback
	);
};

Schema.statics.checkEmailForAbsence = function(email, callback) {
	let User = this;

	User.findOne({ email }, (err, user) => {
		callback(err, !user);
	});
};

Schema.statics.authorize = function(email, password, callback) {
	let User = this;

	async.waterfall(
		[
			callback => User.findOne({ email }, callback),

			(user, callback) => {
				if (user)
					if (user.checkPassword(password))
						return callback(null, user);

				return callback(true, false, {
					status: 401,
					result: {
						msg:
							"Пользователя с таким логином или паролем не существует"
					}
				});
			}
		],
		callback
	);
};

Schema.statics.authorizeById = function(idUser, callback) {
	let User = this;

	User.findOne({ _id: idUser }, callback);
};

const updateGroups = (Schema, user, newGroups, callback) =>
	Schema.update(
		{ _id: user._id },
		{
			$set: {
				groups: newGroups
			}
		},
		err => callback(err, newGroups)
	)

Schema.statics.addGroupAndGetAll = function(user, title, color, callback) {

	//Можно не переживать о том что данные на сервере устарели, при каждом обращении пользователя с jwt, данные о нём берутся из базы
	let newGroups = user.groups.concat({
		id: randToken.generate(16),
		title,
		color,
		tasks: [],
		done: false
	});

	updateGroups(this, user, newGroups, callback);
};

Schema.statics.addTaskAndGetAllGroups = function(
	user,
	idGroup,
	description,
	callback
) {

	//Можно не переживать о том что данные на сервере устарели, при каждом обращении пользователя с jwt, данные о нём берутся из базы
	let newGroups = user.groups.map(group => {
		if (group.id === idGroup)
			group.tasks.push({
				id: randToken.generate(16),
				description,
				done: false
			});

		return group;
	});

	updateGroups(this, user, newGroups, callback);
};

Schema.statics.deleteGroupAndGetAll = function(user, idGroup, callback) {

	//Можно не переживать о том что данные на сервере устарели, при каждом обращении пользователя с jwt, данные о нём берутся из базы
	let newGroups = user.groups.filter(group => group.id !== idGroup);

	updateGroups(this, user, newGroups, callback);
};

Schema.statics.renameGroupAndGetAll = function(user, idGroup, title, callback){
	//Можно не переживать о том что данные на сервере устарели, при каждом обращении пользователя с jwt, данные о нём берутся из базы
	let newGroups = user.groups.map(group => {
		if(group.id === idGroup)
			group.title = title;

		return group;
	});

	updateGroups(this, user, newGroups, callback);
}

Schema.statics.changeDoneTaskAngGetAllGroups = function(user, idGroup, idTask, done, callback){
	let newGroups = user.groups.map(group => {
		if(group.id === idGroup)
			group.tasks = group.tasks.map(task => {
				if(task.id === idTask)
					task.done = done;

				return task;
			})

		return group;
	});

	updateGroups(this, user, newGroups, callback);
}

Schema.statics.deleteTaskAndGetAllGroups = function(user, idGroup, idTask, callback){
	let newGroups = user.groups.map(group => {
		if(group.id === idGroup)
			group.tasks = group.tasks.filter(task => task.id !== idTask)

		return group;
	});

	updateGroups(this, user, newGroups, callback);
}

Schema.statics.changeTaskAngGetAllGroups = function(user, idGroup, idTask, description, callback){
	let newGroups = user.groups.map(group => {
		if(group.id === idGroup)
			group.tasks = group.tasks.map(task => {
				if(task.id === idTask)
					task.description = description;
				return task;
			})

		return group;
	});

	updateGroups(this, user, newGroups, callback);
}

module.exports = mongoose.model("User", Schema);
