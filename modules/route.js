import path from "path";
import config from "config";
import express from "express";
import jwt from "jsonwebtoken";
import bodyParser from "body-parser";
import passport from "../libs/passport";
import * as dataBase from "../middleware/dataBase";
import { matchedData, sanitizeParam } from "express-validator/filter";
import { check, validationResult } from "express-validator/check";

export default function(app) {
	if (!app) var app = express();

	app
		.disable("x-powered-by") //Удаление заголовка express из http

		.use("/public", express.static("./client/public")) //Публичны файлы

		.use(bodyParser.urlencoded({ extended: false })) //Парсер запросов

		.use(bodyParser.json())

		.use(passport.initialize())

		.use("/api?", function(req, res, next) {
			res.setHeader("Access-Control-Allow-Origin", "*"); //Разрешаем кросс-доменные запросы
			res.setHeader(
				"Access-Control-Allow-Headers",
				"Content-Type, Authorization"
			);
			res.setHeader("Access-Control-Allow-Credentials", "true");
			res.setHeader("Access-Control-Allow-Methods", "POST, OPTIONS");
			next();
		})

		.use(
			"/api/checkEmailForAbsence/:email?",
			check("email")
				.withMessage("поле отсутствует")
				.exists(),
			(req, res, next) => {
				const errors = validationResult(req);
				if (!errors.isEmpty()) {
					return res.status(402).json({
						errors: errors.array()
					});
				}
				const email = req.params.email || req.body.email;

				dataBase.checkEmailForAbsence(email, (err, absence, info = {}) => {
					if (err)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});

					if (absence) return res.status(200).json({success: true});

					return res
						.status(400)
						.json({
							errors: [
								{
									msg:
										"Данный адрес электронной почты уже зарегистрирован"
								}
							]
						});
				});
			}
		)

		.post(
			"/api/registration",
			[
				check("givenName")
					.withMessage("должно содержать имя")
					.exists(),
				check("familyName")
					.withMessage("должно содержать фамилию")
					.exists(),
				check("email")
					.isEmail()
					.withMessage("должно содержать email")
					.trim()
					.normalizeEmail(),
				check("password", "Пароль не может быть меньше 5-ти символов")
					.isLength({ min: 5 })
					.matches(/\d/),
				check("passwordConfirmation", "Пароли не совпадают")
					.exists()
					.custom((value, { req }) => value === req.body.password)
			],
			(req, res) => {
				const errors = validationResult(req);
				if (!errors.isEmpty()) {
					return res.status(402).json({
						errors: errors.array()
					});
				}

				dataBase.registrationUser(req.body, function(err, user, info = {}) {
					if (err || !user)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});

					const payload = {
						id: user.id,
						givenName: user.givenName,
						familyName: user.familyName
					};

					const token = jwt.sign(payload, config.get("JwtSecret")); //здесь создается JWT

					return res.status(200).json({
						token: "Bearer " + token,
						givenName: user.givenName,
						familyName: user.familyName,
						email: user.email,
						groups: user.groups
					});
				});
			}
		)

		.post(
			"/api/auth/local",
			[
				check("email")
					.withMessage("должно содержать email")
					.exists(),
				check("password", "Пароль не может быть меньше 5-ти символов")
					.isLength({ min: 5 })
					.matches(/\d/),
			],
			(req, res, next) => {
				const errors = validationResult(req);
				if (!errors.isEmpty()) {
					return res.status(402).json({
						errors: errors.array()
					});
				}

				passport.authenticate("local", function(err, user, info = {}) {
					if (err || !user)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});

					const payload = {
						id: user.id,
						givenName: user.givenName,
						familyName: user.familyName
					};

					const token = jwt.sign(payload, config.get("JwtSecret")); //здесь создается JWT

					return res.status(200).json({
						token: "Bearer " + token,
						givenName: user.givenName,
						familyName: user.familyName,
						email: user.email,
						groups: user.groups
					});
				})(req, res, next);
			}
		)

		.get("/api/userState", (req, res, next) => {
			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				const payload = {
					id: user.id,
					givenName: user.givenName,
					familyName: user.familyName
				};

				const token = jwt.sign(payload, config.get("JwtSecret")); //здесь создается JWT

				return res.status(200).json({
					token: "Bearer " + token,
					givenName: user.givenName,
					familyName: user.familyName,
					email: user.email,
					groups: user.groups
				});
			})(req, res, next);
		})

		.post("/api/addGroup", [
			check("title").exists(),
			check("color").exists()
		], (req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(402).json({
					errors: errors.array()
				});
			}

			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				dataBase.addGroupAndGetAll(user, req.body.title, req.body.color, function(err, groups, info = {}){
					if (err || !groups)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});
					return res.status(200).json(groups);
				})
			})(req, res, next);
		})

		.post("/api/addTask", [
			check("idGroup").exists(),
			check("description").exists()
		], (req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(402).json({
					errors: errors.array()
				});
			}

			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				dataBase.addTaskAndGetAllGroups(user, req.body.idGroup, req.body.description, function(err, groups, info = {}){
					if (err || !groups)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});
					return res.status(200).json(groups);
				})
			})(req, res, next);
		})

		.post("/api/deleteGroup", [
			check("idGroup").exists()
		], (req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(402).json({
					errors: errors.array()
				});
			}

			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				dataBase.deleteGroupAndGetAll(user, req.body.idGroup, function(err, groups, info = {}){
					if (err || !groups)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});
					return res.status(200).json(groups);
				})
			})(req, res, next);
		})

		.post("/api/renameGroup", [
			check("idGroup").exists(),
			check("title").exists()
		], (req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(402).json({
					errors: errors.array()
				});
			}

			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				dataBase.renameGroupAndGetAll(user, req.body.idGroup, req.body.title, function(err, groups, info = {}){
					if (err || !groups)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});
					return res.status(200).json(groups);
				})
			})(req, res, next);
		})

		.post("/api/changeDoneTask", [
			check("idGroup").exists(),
			check("idTask").exists(),
			check("done").exists()
		], (req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(402).json({
					errors: errors.array()
				});
			}

			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				dataBase.changeDoneTaskAngGetAllGroups(user, req.body.idGroup, req.body.idTask, req.body.done, function(err, groups, info = {}){
					if (err || !groups)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});
					return res.status(200).json(groups);
				})
			})(req, res, next);
		})

		.post("/api/deleteTask", [
			check("idGroup").exists(),
			check("idTask").exists()
		], (req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(402).json({
					errors: errors.array()
				});
			}

			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				dataBase.deleteTaskAndGetAllGroups(user, req.body.idGroup, req.body.idTask, function(err, groups, info = {}){
					if (err || !groups)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});
					return res.status(200).json(groups);
				})
			})(req, res, next);
		})

		.post("/api/changeTask", [
			check("idGroup").exists(),
			check("idTask").exists(),
			check("description").exists()
		], (req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(402).json({
					errors: errors.array()
				});
			}

			passport.authenticate("jwt", function(err, user) {
				if (err || !user) return res.status(401).end(); //Не авторизован

				dataBase.changeTaskAngGetAllGroups(user, req.body.idGroup, req.body.idTask, req.body.description, function(err, groups, info = {}){
					if (err || !groups)
						return res
							.status(info.status || 500)
							.json({
								errors: [
									{
										msg:
											info.message ||
											"Непредвиденная ошибка, попробуйте позже"
									}
								]
							});
					return res.status(200).json(groups);
				})
			})(req, res, next);
		})
		
		.use("/api", (req, res) => res.status(405).end())

		.use("/", (req, res, next) => {
			res
				.status(200)
				.send(
					`<!html>
						<head>
							<title>To do list</title>
							<meta charset='utf-8'>
							<meta name="viewport" content="width=device-width, initial-scale=1">
							<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
							<link rel="stylesheet" type="text/css" href="/public/css/style.css">
							<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
						</head>
						<body>
							<div id="app"></div>
							<script src="/public/js/bundle.js"></script>
						</body>
					</html>`
				)
				.end();
		})


	return app;
}
