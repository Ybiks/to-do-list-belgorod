/*Этот модуль призван совмещать интерфейсы разных схем в один, и намного понятней какие аргументы передавать*/

import user from "../schemes/user";

export let authorizeUser = (username, password, callback) =>
	user.authorize(username, password, callback);

export let registrationUser = (data, callback) =>
	user.registration(data, callback);

export let checkEmailForAbsence = (email, callback) =>
	user.checkEmailForAbsence(email, callback);

export let authorizeUserById = (id, callback) =>
	user.authorizeById(id, callback);

export let addGroupAndGetAll = (userObj, title, color, callback) =>
	user.addGroupAndGetAll(userObj, title, color, callback);

export let addTaskAndGetAllGroups = (userObj, idGroup, description, callback) =>
	user.addTaskAndGetAllGroups(userObj, idGroup, description, callback);

export let deleteGroupAndGetAll = (userObj, idGroup, callback) =>
	user.deleteGroupAndGetAll(userObj, idGroup, callback);

export let renameGroupAndGetAll = (userObj, idGroup, title, callback) =>
	user.renameGroupAndGetAll(userObj, idGroup, title, callback);

export let changeDoneTaskAngGetAllGroups = (userObj, idGroup, idTask, done, callback) =>
	user.changeDoneTaskAngGetAllGroups(userObj, idGroup, idTask, done, callback);

export let deleteTaskAndGetAllGroups = (userObj, idGroup, idTask, callback) =>
	user.deleteTaskAndGetAllGroups(userObj, idGroup, idTask, callback);

export let changeTaskAngGetAllGroups = (userObj, idGroup, idTask, description, callback) =>
	user.changeTaskAngGetAllGroups(userObj, idGroup, idTask, description, callback);