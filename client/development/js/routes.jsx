import React from "react";
import { Route, IndexRoute, Redirect } from "react-router";
import App from "./components/App.jsx";
import {Home, Dist, Group, NotFound} from "./components/pages";


export default (
	<Route path="/" component={App}>
		<IndexRoute component={Home} />
		<Route path="/group/:idGroup" component={Group} />
		<Route path="/dist" component={Dist} />
		<Route path="*" component={NotFound} />
	</Route>
);
