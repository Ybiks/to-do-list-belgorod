export default store => next => action => {
	if (action.type !== "LOCAL_STORAGE") return next(action);

	if (action.method === "SET") {
		Object.keys(action.payload).forEach(key =>
			window.localStorage.setItem(key, action.payload[key])
		);
	} else if (action.method === "REMOVE") {
		action.payload.forEach(key => window.localStorage.removeItem(key));
	}

	if(action.next !== void 0)
		store.dispatch(action.next);
};
