import Auth from "./Auth.jsx";
import Danger from "./Danger.jsx";
import Registration from "./Registration.jsx";

export { Auth, Danger, Registration };
