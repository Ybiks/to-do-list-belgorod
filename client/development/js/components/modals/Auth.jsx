import { Formik } from "formik";
import React, { Component } from "react";
import { auth } from "../../api";
import {
	Button,
	Row,
	Col,
	Input,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from "reactstrap";

const LoginForm = ({ _setUser, _mountModal }) => (
	<Formik
		initialValues={{
			email: "",
			password: ""
		}}
		validate={values => {
			let errors = {};
			if (!values.email) {
				errors.email = "Заполните поле";
			} else if (values.email.length < 5) {
				errors.email = "Поле не может содержать меньше 5-ти символов";
			}

			if (!values.password) {
				errors.password = "Заполните поле";
			} else if (values.password.length < 5) {
				errors.password =
					"Поле не может содержать меньше 5-ти символов";
			}

			return errors;
		}}
		onSubmit={(values, { setSubmitting, setErrors }) =>
			auth(values)
				.then(user => {
					_setUser(user);
				})
				.catch(res => {
					try{
						res
							.json()
							.then(json => {
								if (res.status == 401) {
									setErrors({
										password: "Неверный логин или пароль"
									});
									setSubmitting(false);
								} else {
									_mountModal("danger", {
										body: <h4>{json.errors[0].msg}</h4>
									});
								}
							})
					}catch(err){
						_mountModal("danger", {
							body: (
								<h4>
									На сервере произошла ошибка, попробуйте
									позже
								</h4>
							)
						});
						
					}
				})
		}
		render={({
			values,
			errors,
			touched,
			handleChange,
			handleBlur,
			handleSubmit,
			isSubmitting
		}) => (
			<form onSubmit={handleSubmit}>
				<ModalBody>
					<Row>
						<Col xs={{ size: 8, offset: 2 }}>
							<br />
							<Input
								name="email"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.email}
								placeholder="Логин"
							/>
							{touched.email &&
								errors.email && (
									<div className="text-danger">
										{errors.email}
									</div>
								)}
							<br />
						</Col>
						<Col xs={{ size: 8, offset: 2 }}>
							<Input
								type="password"
								name="password"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.password}
								placeholder="Пароль"
							/>
							{touched.password &&
								errors.password && (
									<div className="text-danger">
										{errors.password}
									</div>
								)}
							<br />
						</Col>
					</Row>
				</ModalBody>
				<ModalFooter>
					<Button
						color="link"
						onClick={() => _mountModal("registration")}
					>
						Регистрация
					</Button>
					<Button
						color="primary"
						type="submit"
						disabled={isSubmitting}
					>
						Войти
					</Button>
				</ModalFooter>
			</form>
		)}
	/>
);

export default props => (
	<Modal
		isOpen={props.modalsState.hasOwnProperty("auth")}
		toggle={() => props._unmountModal("auth")}
		className={props.className}
	>
		<ModalHeader toggle={() => props._unmountModal("auth")}>
			Вход
		</ModalHeader>
		<LoginForm _setUser={props._setUser} _mountModal={props._mountModal} />
	</Modal>
);
