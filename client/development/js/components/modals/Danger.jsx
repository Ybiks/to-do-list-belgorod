import React, { Component } from "react";
import {
	Button,
	Row,
	Col,
	Input,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from "reactstrap";

const getValueWithoutError = (object, field) => object ? object[field] : undefined;

export default props => (
	<Modal
		isOpen={props.modalsState.hasOwnProperty("danger")}
		toggle={() => props._unmountModal("danger")}
		className={props.className}
	>
		<ModalHeader toggle={() => props._unmountModal("danger")}>
			{getValueWithoutError(props.modalsState.danger, "header") || "Ошибка"}
		</ModalHeader>
		<ModalBody>
			{getValueWithoutError(props.modalsState.danger, "body")}
		</ModalBody>
		<ModalFooter>
			{getValueWithoutError(props.modalsState.danger, "footer")}
		</ModalFooter>
	</Modal>
);
