import { Formik } from "formik";
import React, { Component } from "react";
import { registration, checkEmailForAbsence } from "../../api";
import {
	Button,
	Row,
	Col,
	Input,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from "reactstrap";

const LoginForm = ({ _setUser, _mountModal }) => (
	<Formik
		initialValues={{
			givenName: "",
			familyName: "",
			email: "",
			password: "",
			passwordConfirmation: ""
		}}
		validate={values => new Promise((resolve, reject) => {

			let errors = {};

			if (!values.givenName) {
				errors.givenName = "Заполните поле";
			} else if (values.givenName.length < 2) {
				errors.givenName = "Имя не может быть короче 2-х символов";
			}

			if (!values.familyName) {
				errors.familyName = "Заполните поле";
			} else if (values.familyName.length < 5) {
				errors.familyName =
					"Фамилия не может быть короче 5-ти символов";
			}

			if (!values.email) {
				errors.email = "Заполните поле";
			} else if (values.email.length < 5) {
				errors.email = "Поле не может содержать меньше 5-ти символов";
			} else if (
				!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
			) {
				errors.email = "Поле должно содержать адрес почтового ящика";
			}

			if (!values.password) {
				errors.password = "Заполните поле";
			} else if (values.password.length < 5) {
				errors.password =
					"Поле не может содержать меньше 5-ти символов";
			}

			if (!values.passwordConfirmation) {
				errors.passwordConfirmation = "Заполните поле";
			} else if (values.passwordConfirmation !== values.password) {
				errors.passwordConfirmation = "Пароли не совпадают";
			}

			if (Object.keys(errors).length) {
				reject(errors)
			} else {
				return checkEmailForAbsence(values.email)
					.then(() => resolve())
					.catch(res => {
						try{
							res.json()
								.then(json => {
									if(res.status == 400){
										errors.email = json.errors[0].msg;
										reject(errors);
									}else{
										_mountModal('danger', {
											body: <h4>{json.errors[0].msg}</h4>
										})
										resolve();
									}
								})
						}catch(err){
							_mountModal("danger", {
								body: (
									<h4>
										На сервере произошла ошибка, попробуйте
										позже
									</h4>
								)
							});
							
						}
					})
			}
		})}
		onSubmit={(values, { setSubmitting, setErrors }) => {
			registration(values)
				.then(user => {
					_setUser(user);
				})
				.catch(res => {
					try{
						res.json()
							.then(json => {
								_mountModal('danger', {
									body: <h4>{json.errors[0].msg}</h4>
								})
								resolve();
							})
					}catch(err){
						_mountModal("danger", {
							body: (
								<h4>
									На сервере произошла ошибка, попробуйте
									позже
								</h4>
							)
						});
						
					}
				});
		}}
		render={({
			values,
			errors,
			touched,
			handleChange,
			handleBlur,
			handleSubmit,
			isSubmitting
		}) => (
			<form onSubmit={handleSubmit}>
				<ModalBody>
					<Row>
						<Col xs={{ size: 8, offset: 2 }}>
							<Input
								name="givenName"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.givenName}
								placeholder="Имя"
							/>
							{touched.givenName &&
								errors.givenName && (
									<div className="text-danger">
										{errors.givenName}
									</div>
								)}
							<br />
						</Col>
						<Col xs={{ size: 8, offset: 2 }}>
							<Input
								name="familyName"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.familyName}
								placeholder="Фамилия"
							/>
							{touched.familyName &&
								errors.familyName && (
									<div className="text-danger">
										{errors.familyName}
									</div>
								)}
							<br />
						</Col>
						<Col xs={{ size: 8, offset: 2 }}>
							<Input
								name="email"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.email}
								placeholder="E-mail"
							/>
							{touched.email &&
								errors.email && (
									<div className="text-danger">
										{errors.email}
									</div>
								)}
							<br />
						</Col>
						<Col xs={{ size: 8, offset: 2 }}>
							<Input
								type="password"
								name="password"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.password}
								placeholder="Пароль"
							/>
							{touched.password &&
								errors.password && (
									<div className="text-danger">
										{errors.password}
									</div>
								)}
							<br />
						</Col>
						<Col xs={{ size: 8, offset: 2 }}>
							<Input
								type="password"
								name="passwordConfirmation"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.passwordConfirmation}
								placeholder="Повторите пароль"
							/>
							{touched.passwordConfirmation &&
								errors.passwordConfirmation && (
									<div className="text-danger">
										{errors.passwordConfirmation}
									</div>
								)}
							<br />
						</Col>
					</Row>
				</ModalBody>
				<ModalFooter>
					<Button
						color="primary"
						type="submit"
						disabled={isSubmitting}
					>
						Регистрация
					</Button>
				</ModalFooter>
			</form>
		)}
	/>
);

export default props => (
	<Modal
		isOpen={props.modalsState.hasOwnProperty("registration")}
		toggle={() => props._unmountModal("registration")}
		className={props.className}
	>
		<ModalHeader toggle={() => props._unmountModal("registration")}>
			Вход
		</ModalHeader>
		<LoginForm
			_setUser={props._setUser}
			_mountModal={props._mountModal}
		/>
	</Modal>
);
