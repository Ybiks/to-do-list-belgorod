import Home from './Home.jsx';
import Dist from './Dist.jsx';
import Group from './Group.jsx';
import NotFound from './NotFound.jsx';

export {
	Home,
	Dist,
	Group,
	NotFound,
}