import React from 'react'

class NotFound extends React.Component{
	render(){
		return (
			<div className="page-not-found">
				<div className="page-not-found__container">
					<h1 className="page-not-found__title">404</h1>
					<h2 className="page-not-found__description">СТРАНИЦА НЕ НАЙДЕНА</h2>
				</div>
			</div>
		)
	}
}

export default NotFound;