import React, {Component} from 'react';
import {Router, Redirect, browserHistory} from 'react-router';
import {HistoryElement} from '../../libs/HistoryElement.jsx';
import {Container, Row, Col, Button, Badge} from 'reactstrap';


class Home extends Component{
	constructor(props){
		super(props);
	}

	//Определяем, зарегистрирован ли пользователь, если да, то редиректим на его группу
	doNeedToRedirect(props){
		if(props.state.user)
			browserHistory.push(`/group/${props.state.user.groups[0].id}`);
	}

	componentWillMount(){
		this.doNeedToRedirect(this.props);
	}

	componentWillUpdate(newProps){
		this.doNeedToRedirect(newProps)
	}

	render(){
		return <HistoryElement
			pathname="/home"
		>
			<Container>
				<br/>
				<h1>Тестовое задание</h1>
				<p>Мной было разработанно react приложение и API(с ужасным интерфейсом, так как спешил). Я бы мог прекрутить тестирование на mocha и chai, но не хватило времени.</p>
				<p>Была использованна React библиотека reactstrap предоставляющая bootstrap 4 для react. Выбор пал на неё по перечине того, что официальный react-bootstrap поддерживает только 3-ю версию.</p>
				<p>Хочу предложить вам сравнение, вот <a href="https://remaster-prometeus.herokuapp.com/">сайт</a> который я разработал с использованием css framework materialize и библиотеки remodal</p>
			</Container>
		</HistoryElement>
	}
}

export default Home;