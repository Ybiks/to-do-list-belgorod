import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {HistoryElement} from '../../libs/HistoryElement.jsx';
import {Container} from 'reactstrap';


export default class Dist extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return (
			<HistoryElement
				pathname="/dist"
			>
				<br/>
				<Container>
					<h1>Репозиторий</h1>
					<p>Исходники расположенны на Bitbucket по <a href="https://bitbucket.org/Ybiks/to-do-list-belgorod">адресу</a>.</p>
				</Container>
			</HistoryElement>
		)
	}
}