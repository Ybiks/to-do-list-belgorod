import { addGroup } from "../../api";
import { AddTask, AddGroup, ActionForGroup, Task } from "../";
import React, { Component } from "react";
import { browserHistory } from "react-router";
import { LinkContainer } from "react-router-bootstrap";
import { Container, Row, Col, Button, ListGroup, ListGroupItem } from "reactstrap";
import { HistoryElement } from "../../libs/HistoryElement.jsx";

const ButtonGroup = ({ id, title, color }) => (
	<LinkContainer to={`/group/${id}`}>
		<Button color="link" className="link-group__btn btn-block text-left">
			<div
				className="link-group__icon list-inline-item"
				style={{ backgroundColor: color }}
			/>
			{title}
		</Button>
	</LinkContainer>
);

export default class Group extends Component {
	constructor(props) {
		super(props);
	}

	//При переходе по "битой" ссылке отредиректит на первую группу в списке.
	//Но если группы и вовсе отсутствуют, то не будет редиректить. В рендере это обрабатывается.
	doNeedToRedirect(props){
		if (!props.state.user) {
			//Если пользователь отсутствует, редирект на главную
			browserHistory.push("/");
		} else {
			const idActiveGroup = props.state.params.idGroup;
			const activeGroupe = props.state.user.groups.filter(
				group => idActiveGroup === group.id
			)[0];
			if(!activeGroupe){
				//Если мы находимся на странице группы которой уже не существует
				if(props.state.user.groups[0]){
					//Если имеется первая страница, то редиректим на неё
					browserHistory.push(`/group/${props.state.user.groups[0].id}`);
				}
			}
		}
	}

	componentWillMount(){
		this.doNeedToRedirect(this.props);
	}

	componentWillUpdate(newProps){
		this.doNeedToRedirect(newProps)
	}

	render() {
		//Если пользователь отсутствует и мы вызвали редирект, render всё равно запустится, поэтому, пускай вернёт null
		if(!this.props.state.user){
			return null;
		}

		//Получаем активную группу
		let activeGroupe = this.props.state.user.groups.filter(
			group => group.id === this.props.state.params.idGroup
		)[0];

		//Сюда неочевидно может попасть уже удалённая группа, это произойдёт в том случае если удалённая группа была последней, и мы должны это правильно отразить
		return (
			<HistoryElement
				pathname="/group"
				style={{
					overflowY: "auto",
					height: "calc(100% - 62px)",
					backgroundColor: "#edeef0"
				}}
			>
				<Container>
					<Row className="to-do-list">
						<Col md={{ size: 3 }} className="link-group">
							<br />
							Группы
							<hr />
							{this.props.state.user.groups.map(group => 
								<ButtonGroup
									key={group.id}
									id={group.id}
									title={group.title}
									color={group.color}
								/>
							)}
							{
								this.props.state.user.groups.length
									? <hr/>
									: null
							}
							<AddGroup
								token={this.props.state.user.token}
								_mountModal={this.props.state._mountModal}
								_updateGroups={
									this.props.state._updateGroups
								}
							/>
							<br />
						</Col>
						<Col
							md={{ size: 9 }}
							className="tasks-block"
						>
							{
								//Обёрнуто функциями, что бы прежде времени не запрашивать несуществующие поля объектов
								(activeGroupe
									? () =>
									(
										//Если существует просматриваемая группа
										<div>
											<br />
											<ActionForGroup
												group={activeGroupe}
												token={this.props.state.user.token}
												_mountModal={this.props.state._mountModal}
												_deleteGroup={this.props.state._deleteGroup}
												_updateGroups={
													this.props.state._updateGroups
												}
											/>
											<hr />
											<ListGroup>
											{
												activeGroupe.tasks.map(task =>
													<Task
														task={task}
														key={task.id}
														group={activeGroupe}
														token={this.props.state.user.token}
														_deleteTask={this.props.state._deleteTask}
														_mountModal={this.props.state._mountModal}
														_changeDoneTask={this.props.state._changeDoneTask}
														_updateGroups={
															this.props.state._updateGroups
														}
													/>
												)
											}
											</ListGroup>
											{
												activeGroupe.tasks.length
													? <hr/>
													: null
											}
											<AddTask
												token={this.props.state.user.token}
												idGroup={activeGroupe.id}
												_mountModal={this.props.state._mountModal}
												_updateGroups={
													this.props.state._updateGroups
												}
											/>
										</div>
									)
									: () => (
										//Если группы отсутствуют
										<div>
											<br/>
											<h5 className="text-center">Добавте группу, что бы начать работать</h5>
										</div>
									)
								)()
							}
						</Col>
					</Row>
				</Container>
			</HistoryElement>
		)
}
}