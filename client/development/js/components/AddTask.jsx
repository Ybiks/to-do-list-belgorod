import { Formik } from "formik";
import { addTask } from "../api";
import React, { Component } from "react";
import { Input, InputGroup, InputGroupAddon, Button } from "reactstrap";

export default class AddTask extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: false
		};
		this.changeActive = this.changeActive.bind(this);
	}

	changeActive() {
		this.setState({ active: !this.state.active });
	}

	//При dispatch функционал компонента скрывается
	componentWillReceiveProps() {
		this.setState({ active: false });
	}

	render() {
		if (this.state.active) {
			return (
				<Formik
					className="text-right"
					initialValues={{
						description: ""
					}}
					validate={values => {
						let errors = {};
						if (!values.description) {
							errors.description = "Заполните поле";
						}
						return errors;
					}}
					onSubmit={(values, { setSubmitting, setErrors }) => {
						addTask(
							this.props.token,
							this.props.idGroup,
							values.description
						)
							.then(groups => {
								this.props._updateGroups(groups);
							})
							.catch(res => {
								this.props._mountModal("danger", {
									body: (
										<h4>
											На сервере произошла ошибка,
											попробуйте позже
										</h4>
									)
								});
							});
					}}
					render={({
						values,
						errors,
						touched,
						handleChange,
						handleBlur,
						handleSubmit,
						isSubmitting
					}) => (
						<form onSubmit={handleSubmit}>
							<Input
								name="description"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.description}
								placeholder="Описание задачи"
							/>
							{touched.description &&
								errors.description && (
									<div className="text-danger">
										{errors.description}
									</div>
								)}
							<br />
							<div className="float-right">
								<Button
									size="sm"
									color="link"
									onClick={this.changeActive}
								>
									Отмена
								</Button>
								<Button
									size="sm"
									type="submit"
									color="primary"
									disabled={isSubmitting}
								>
									Добавить
								</Button>
							</div>
						</form>
					)}
				/>
			);
		} else {
			return (
				<Button
					size="sm"
					className="float-right"
					onClick={this.changeActive}
				>
					Создать задачу
				</Button>
			);
		}
	}
}
