import ReactDOM from "react-dom";
import { connect } from "react-redux";
import * as actions from "../actions/";
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { LinkContainer } from "react-router-bootstrap";
import { Auth, Registration, Danger } from "./modals";
import {
	Container,
	Button,
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink
} from 'reactstrap';

class App extends Component {
	constructor(props){
		super(props);
		this.toggleNavbar = this.toggleNavbar.bind(this);
		this.state = {
			collapsed: true
		};
	}
	toggleNavbar() {
		this.setState({
			collapsed: !this.state.collapsed
		});
	}

	render() {
		const renderIfIsUser = (resolveComponent, rejectComponent) => 
			this.props.user
				? resolveComponent || ""
				: rejectComponent || "";

		return (
			<div>
				{
					renderIfIsUser(
						/*Для зарегистрированного*/
						<Navbar light expand="md" className="gradient-1 z-depth-2">
							<Container>
								<LinkContainer to="/">
									<NavLink href="/" className="d-inline-block">
										<h5 className="text-white">To do list</h5>
									</NavLink>
								</LinkContainer>
								<Nav navbar className="float-right d-none d-md-block list-inline">
									<NavItem className="list-inline-item">
										<LinkContainer to="/dist">
											<NavLink href="/dist" className="text-white">Репозиторий</NavLink>
										</LinkContainer>
									</NavItem>
									<NavItem className="list-inline-item">
										<Button color="link" className="text-white" onClick={this.props._logoutUser}>Выйти</Button>
									</NavItem>
								</Nav>
								<NavbarToggler onClick={this.toggleNavbar} className="costom-style-btn float-right mr-2 d-md-none" />
							</Container>
							<Collapse isOpen={!this.state.collapsed} navbar>
								<Nav navbar className="d-md-none">
									<LinkContainer to="/dist">
										<NavLink href="/dist" className="text-white">Репозиторий</NavLink>
									</LinkContainer>
									<NavItem>
										<Button color="link" className="text-white" onClick={this.props._logoutUser}>Выйти</Button>
									</NavItem>
								</Nav>
							</Collapse>
						</Navbar>,
						/*Для незарегистрированного*/
						<div>
							<Auth
								modalsState={this.props.modals}
								_setUser={this.props._setUser}
								_mountModal={this.props._mountModal}
								_unmountModal={this.props._unmountModal}
							/>
							<Registration
								modalsState={this.props.modals}
								_setUser={this.props._setUser}
								_mountModal={this.props._mountModal}
								_unmountModal={this.props._unmountModal}
							/>
							<Danger
								modalsState={this.props.modals}
								_setUser={this.props._setUser}
								_mountModal={this.props._mountModal}
								_unmountModal={this.props._unmountModal}
							/>
							<Navbar light expand="md" className="gradient z-depth-2">
								<Container>
									<NavbarBrand href="/" className="mr-auto text-white">To do list</NavbarBrand>
									<Nav navbar className="float-right d-none d-md-block list-inline">
										<NavItem className="list-inline-item">
											<LinkContainer to="/dist">
												<NavLink href="/dist" className="text-white">Исходники</NavLink>
											</LinkContainer>
										</NavItem>
										<NavItem className="list-inline-item">
											<Button color="link" className="text-white" onClick={() => this.props._mountModal('auth')}>Войти</Button>
										</NavItem>
									</Nav>
									<NavbarToggler onClick={this.toggleNavbar} className="costom-style-btn float-right mr-2 d-md-none" />
								</Container>
								<Collapse isOpen={!this.state.collapsed} navbar>
									<Nav navbar className="d-md-none">
										<LinkContainer to="/dist">
											<NavLink href="/dist" className="text-white">Исходники</NavLink>
										</LinkContainer>
										<NavItem>
											<Button color="link" className="text-white" onClick={() => this.props._mountModal('auth')}>Войти</Button>
										</NavItem>
									</Nav>
								</Collapse>
							</Navbar>
						</div>
					)
				}
				{
					this.props.children
						? React.cloneElement(this.props.children, {
							state: this.props
						})
						: null
				}
			</div>
		)
	}
}

export default connect(
	state => ({
		user: state.user,
		modals: state.modals
	}),
	dispatch => bindActionCreators(actions, dispatch)
)(App);
