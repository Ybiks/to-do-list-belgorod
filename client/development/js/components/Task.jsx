import { Formik } from "formik";
import { changeTask } from "../api";
import React, { Component } from "react";
import { CirclePicker } from "react-color";
import { Link, browserHistory } from "react-router";
import { LinkContainer } from "react-router-bootstrap";
import {
	Input,
	InputGroup,
	InputGroupAddon,
	Button,
	ButtonDropdown,
	DropdownToggle,
	DropdownItem,
	DropdownMenu,
	ListGroupItem
} from "reactstrap";

export default class Task extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: false,
			dropdownOpen: false
		};
		this.changeActive = this.changeActive.bind(this);
		this.deleteTask = this.deleteTask.bind(this);
		this.toggle = this.toggle.bind(this);
		this.changeDoneTask = this.changeDoneTask.bind(this);
	}

	changeActive() {
		this.setState({ active: !this.state.active });
	}
	toggle() {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen
		});
	}
	deleteTask() {
		this.props._deleteTask(
			this.props.token,
			this.props.group.id,
			this.props.task.id
		);
	}

	changeDoneTask(e) {
		this.props._changeDoneTask(
			this.props.token,
			this.props.group.id,
			this.props.task.id,
			e.target.checked
		);
	}

	//При переходе на другую группу скрываем
	componentWillReceiveProps() {
		this.setState({ active: false });
	}

	render() {
		if (this.state.active) {
			return (
				<Formik
					className="text-right"
					initialValues={{
						description: this.props.task.description
					}}
					validate={values => {
						let errors = {};
						if (!values.description) {
							errors.description = "Заполните поле";
						}
						return errors;
					}}
					onSubmit={(values, { setSubmitting, setErrors }) => {
						changeTask(
							this.props.token,
							this.props.group.id,
							this.props.task.id,
							values.description
						)
							.then(groups => {
								this.props._updateGroups(groups);
							})
							.catch(res => {
								this.props._mountModal("danger", {
									body: (
										<h4>
											На сервере произошла ошибка,
											попробуйте позже
										</h4>
									)
								});
							});
					}}
					render={({
						values,
						errors,
						touched,
						handleChange,
						handleBlur,
						handleSubmit,
						isSubmitting
					}) => (
						<form onSubmit={handleSubmit}>
							<ListGroupItem className="d-block">
								<Input
									name="description"
									onChange={handleChange}
									onBlur={handleBlur}
									value={values.description}
									placeholder="Описание задачи"
								/>
								{touched.description &&
									errors.description && (
										<div className="text-danger">
											{errors.description}
										</div>
									)}
								<br />
								<div className="float-right">
									<Button
										size="sm"
										color="link"
										onClick={this.changeActive}
									>
										Отмена
									</Button>
									<Button
										size="sm"
										type="submit"
										color="primary"
										disabled={isSubmitting}
									>
										Сохранить
									</Button>
								</div>
								<br />
							</ListGroupItem>
						</form>
					)}
				/>
			);
		} else {
			return (
				<ListGroupItem className="d-inline-block">
					<Input
						type="checkbox"
						className="custom-checkbox"
						checked={this.props.task.done}
						onChange={this.changeDoneTask}
					/>
					<span>{this.props.task.description}</span>
					<ButtonDropdown
						isOpen={this.state.dropdownOpen}
						toggle={this.toggle}
						className="float-right"
					>
						<DropdownToggle caret size="sm">
							Действия
						</DropdownToggle>
						<DropdownMenu>
							<DropdownItem onClick={this.changeActive}>
								Изменить
							</DropdownItem>
							<DropdownItem onClick={this.deleteTask}>
								Удалить
							</DropdownItem>
						</DropdownMenu>
					</ButtonDropdown>
				</ListGroupItem>
			);
		}
	}
}
