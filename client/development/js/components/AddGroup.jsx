import { Formik } from "formik";
import { addGroup } from "../api";
import React, { Component } from "react";
import { CirclePicker } from "react-color";
import { browserHistory } from "react-router";
import { Input, InputGroup, InputGroupAddon, Button } from "reactstrap";

export default class AddGroup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: false,
			showPicker: false,
			color: "#f44336"
		};

		this.changeActive = this.changeActive.bind(this);
		this.showPicker = this.showPicker.bind(this);
		this.handleChangeComplete = this.handleChangeComplete.bind(this);
	}

	changeActive() {
		this.setState({active: !this.state.active});
	}

	showPicker() {
		this.setState({ showPicker: true });
	}

	handleChangeComplete(color) {
		this.setState({ showPicker: false, color: color.hex });
	}

	//При dispatch функционал компонента скрывается
	componentWillReceiveProps() {
		this.setState({ active: false });
	}

	render() {
		if (this.state.active) {
			return (
				<Formik
					className="text-right"
					initialValues={{
						title: ""
					}}
					validate={values => {
						let errors = {};
						if (!values.title) {
							errors.title = "Заполните поле";
						}
						return errors;
					}}
					onSubmit={(values, { setSubmitting, setErrors }) => {
						addGroup(
							this.props.token,
							values.title,
							this.state.color
						)
							.then(groups => {
								this.props._updateGroups(groups);
							})
							.catch(res => {
								this.props._mountModal("danger", {
									body: (
										<h4>
											На сервере произошла ошибка,
											попробуйте позже
										</h4>
									)
								});
							});
					}}
					render={({
						values,
						errors,
						touched,
						handleChange,
						handleBlur,
						handleSubmit,
						isSubmitting
					}) => (
						<form onSubmit={handleSubmit}>
							{this.state.showPicker ? (
								<div className="picker-conteiner">
									<CirclePicker
										color={this.state.color}
										onChangeComplete={
											this.handleChangeComplete
										}
									/>
								</div>
							) : (
								""
							)}
							<InputGroup>
								<InputGroupAddon
									addonType="prepend"
									style={{
										backgroundColor: this.state.color
									}}
									onClick={this.showPicker}
								>
									&nbsp;&nbsp;&nbsp;&nbsp;
								</InputGroupAddon>
								<Input
									name="title"
									onChange={handleChange}
									onBlur={handleBlur}
									value={values.title}
									placeholder="Название"
								/>
							</InputGroup>
							{touched.title &&
								errors.title && (
									<div className="text-danger">
										{errors.title}
									</div>
								)}
							<br />
							<div className="float-right">
								<Button size="sm" color="link" onClick={this.changeActive}>
									Отмена
								</Button>
								<Button
									size="sm"
									type="submit"
									color="primary"
									disabled={isSubmitting}
								>
									Добавить
								</Button>
							</div>
						</form>
					)}
				/>
			);
		} else {
			return (
				<Button
					size="sm"
					color="primary"
					className="float-right"
					onClick={this.changeActive}
				>
					Добавить группу
				</Button>
			);
		}
	}
}
