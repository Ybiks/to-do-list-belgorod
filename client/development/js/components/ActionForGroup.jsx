import { Formik } from "formik";
import { renameGroup } from "../api";
import React, { Component } from "react";
import {
	Input,
	Button,
	ButtonDropdown,
	DropdownToggle,
	DropdownItem,
	DropdownMenu
} from "reactstrap";

export default class ActionForGroup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: false,
			dropdownOpen: false
		};
		this.changeActive = this.changeActive.bind(this);
		this.deleteGroup = this.deleteGroup.bind(this);
		this.toggle = this.toggle.bind(this);
	}

	changeActive() {
		this.setState({ active: !this.state.active });
	}
	toggle() {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen
		});
	}
	deleteGroup() {
		this.props._deleteGroup(this.props.token, this.props.group.id);
	}

	//При dispatch функционал компонента скрывается
	componentWillReceiveProps() {
		this.setState({ active: false });
	}

	render() {
		if (this.state.active) {
			return (
				<Formik
					className="text-right"
					initialValues={{
						title: this.props.group.title
					}}
					validate={values => {
						let errors = {};
						if (!values.title) {
							errors.title = "Заполните поле";
						}
						return errors;
					}}
					onSubmit={(values, { setSubmitting, setErrors }) => {
						renameGroup(
							this.props.token,
							this.props.group.id,
							values.title
						)
							.then(groups => {
								this.props._updateGroups(groups);
							})
							.catch(res => {
								this.props._mountModal("danger", {
									body: (
										<h4>
											На сервере произошла ошибка,
											попробуйте позже
										</h4>
									)
								});
							});
					}}
					render={({
						values,
						errors,
						touched,
						handleChange,
						handleBlur,
						handleSubmit,
						isSubmitting
					}) => (
						<form onSubmit={handleSubmit}>
							<Input
								name="title"
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.title}
								placeholder="Название группы"
							/>
							{touched.title &&
								errors.title && (
									<div className="text-danger">
										{errors.title}
									</div>
								)}
							<br />
							<div className="float-right">
								<Button
									size="sm"
									color="link"
									onClick={this.changeActive}
								>
									Отмена
								</Button>
								<Button
									size="sm"
									type="submit"
									color="primary"
									disabled={isSubmitting}
								>
									Переименовать
								</Button>
							</div>
							<br />
						</form>
					)}
				/>
			);
		} else {
			return (
				<div>
					<h4 className="d-inline-block">{this.props.group.title}</h4>
					<ButtonDropdown
						isOpen={this.state.dropdownOpen}
						toggle={this.toggle}
						className="float-right"
					>
						<DropdownToggle caret size="sm" color="success">
							Действия
						</DropdownToggle>
						<DropdownMenu>
							<DropdownItem onClick={this.changeActive}>
								Переименовать
							</DropdownItem>
							<DropdownItem onClick={this.deleteGroup}>
								Удалить
							</DropdownItem>
						</DropdownMenu>
					</ButtonDropdown>
					<br />
				</div>
			);
		}
	}
}
