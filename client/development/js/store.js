import { createStore, applyMiddleware } from "redux";
import promisesMiddleware from "./middlewares/promises";
import localStorageMiddleware from "./middlewares/localStorage";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";
import reducer from "./reducers";


let middleware = [ promisesMiddleware, localStorageMiddleware ];
if (process.env.NODE_ENV !== 'production') {
	let logger = require("redux-logger").logger;
	middleware = [ ...middleware, logger ];
}

const createStoreWithMiddleware = applyMiddleware(...middleware)(
	createStore
);

export default function(initialState = {}) {
	return createStoreWithMiddleware(reducer, initialState);
}
