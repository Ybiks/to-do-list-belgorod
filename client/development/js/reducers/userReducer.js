export default function(state = null, action) {
	switch (action.type) {
		case "SET_USER":
			return {...state, ...action.payload};

		case "LOGOUT_USER":
			return null;

		case "UPDATE_GROUPS":
			return {...state, groups: action.payload};

		case "DELETE_GROUP_LOADED":
			return {...state, groups: action.data};

		case "DELETE_TASK_LOADED":
			return {...state, groups: action.data};

		case "CHANGE_DONE_TASK_LOADED":
			return {...state, groups: action.data};

		default:
			return state;
	}
}
