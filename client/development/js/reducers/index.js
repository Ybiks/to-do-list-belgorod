import customRouterReducer from "./customRouterReducer";
import { routerReducer } from "react-router-redux";
import { combineReducers } from "redux";
import modals from "./modalsReducer";
import user from "./userReducer";

export default combineReducers({
	routing: routerReducer,
	customRouterReducer,
	modals,
	user
});
