import { deletePath } from "../libs/HistoryElement.jsx";

export default function(state = null, action) {
	if (action.type !== "@@router/LOCATION_CHANGE") return state;

	if (action.payload.action === "PUSH") deletePath(action.payload.pathname);

	return state;
}
