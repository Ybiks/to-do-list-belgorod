/*Модуль для сборки бандла*/
import { polyfill } from "es6-promise";
polyfill();

import React from "react";
import config from "./config";
import ReactDOM from "react-dom";
import { getUserState } from "./api";
import routes from "./routes.jsx";
import createStore from "./store";
import fetch from "isomorphic-fetch";
import { Provider } from "react-redux";
import { browserHistory, Router } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";

let render = (state) => {
	const store = createStore(state);
	const history = syncHistoryWithStore(browserHistory, store);

	ReactDOM.render(
		<Provider store={store}>
			<Router history={history}>{routes}</Router>
		</Provider>,
		document.getElementById("app")
	);
}

const token = window.localStorage.getItem("token");

if (token) {
	getUserState(token)
		.then(userState => render({user: userState}))
		.catch(error => {
			//log error
			window.localStorage.removeItem("token")
			render({});
		})
} else {
	render({});
}

