const VOCABULARY = {
	ru: {
		'Login': 'Вход',
		'Register': 'Регистрация',
		'Login to your account': 'Вход в аккаунт',
		'Username': 'Логин',
		'Password': 'Пароль',
		'Forgot your password?': 'Забыли пароль?',
		'The value of the field cannot be less than 5 characters': 'Значение поля не может быть короче 5-ти символов',
		'The value of the field cannot be less than 6 characters': 'Значение поля не может быть короче 6-ти символов',
		'Create an account': 'Создать аккаунт',
		'Passwords Don\'t Match': 'Пароли не совпадают',
		'Confirm password': 'Повторите пароль',
		'Your Email': 'Ваша электронная почта',
		'Wrong login or password': 'Неверный логин или пароль',
		'To retry': 'Повторить попытку',
		'Username or E-mail': 'Логин или электронная почта',
		'Your Username or Email': 'Ваш логин мли электронная почта',
		'Send a message to the mail': 'Отправить сообщение на почту',
		'Edit account': 'Редактировать аккаунт',
		'Home': 'Главная',
		'Tenant': 'Арендовать',
		'Lease': 'Сдавать в аренду'
	}
}

function getLanguageOfCookie(){
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + 'language'.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function determineLanguage(){
	if(typeof window !== "undefined")
		if(document.cookie){
			var result = getLanguageOfCookie();
			if(result)
				return result;
		}

	if(typeof navigator !== "undefined")
		if(navigator.language)
			return navigator.language;

	return 'ru';
}

function transfer(language){
	return function(text){
		if(text == 'language') return language;//Для определеняия какой язык используется

		if(language == 'en') return text;//Если фраза на английском, мы её не переводим

		var transfer = VOCABULARY[language][text];

		if(transfer == void 0){
			//В словаре нет перевода, сообщаем об ошибки
			console.log('Ошибка поиска локализации', text, language);
			return text;
		}else{
			return transfer;
		}
	}
}

function setLanguageInCookie(value) {
	
	var d = new Date();
	d.setTime(d.getTime() + 2592000 * 1000);
	var expires = d.toUTCString();

	value = encodeURIComponent(value);

	var updatedCookie = "language=" + value + "; " + expires;

	document.cookie = updatedCookie;

	return value;
}

export default {
	determineLanguage,
	transfer,
	setLanguageInCookie
}