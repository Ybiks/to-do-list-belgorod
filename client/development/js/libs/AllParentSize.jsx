import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class extends Component {
	constructor(props) {
		super(props);
		this.resize = this.resize.bind(this);
	}

	resize() {
		let element = ReactDOM.findDOMNode(this.searchPanel);
		let parent = element.parentNode;

		element.style.width = parent.clientWidth + "px";
		element.style.height = parent.clientHeight + "px";
	}

	componentDidMount() {
		this.resize();
		window.addEventListener("resize", this.resize);
	}
	componentWillUnmount() {
		window.removeEventListener("resize", this.resize);
	}

	render() {
		return (
			<div
				{...{
					style: this.props.style,
					className: this.props.className
				}}
				ref={ref => (this.searchPanel = ref)}
			>
				{this.props.children}
			</div>
		);
	}
}
