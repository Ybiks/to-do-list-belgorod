import * as api from "../api";

export let _mountModal = (name, settings) => ({
	type: "MOUNT_MODAL",
	payload: { name, settings }
});

export let _unmountModal = contentLabel => ({
	type: "UNMOUNT_MODAL",
	payload: contentLabel
});

export let _setUser = user => ({
	type: "LOCAL_STORAGE", //Вызов middleware
	method: "SET", //Что хотим сделать в middleware
	payload: { token: user.token }, //Поля которые мы хотим сохранить в локальном хранилище
	next: {
		type: "SET_USER",
		payload: user
	} //Следующий диспатч
});

export let _logoutUser = () => ({
	type: "LOCAL_STORAGE", //Вызов middleware
	method: "REMOVE", //Что хотим сделать в middleware
	payload: ["token"], //Поля которые мы хотим удалить в локальном хранилище
	next: {
		type: "LOGOUT_USER"
	} //Следующий диспатч
});

export let _updateGroups = groups => ({
	type: "UPDATE_GROUPS",
	payload: groups
})

export let _changeDoneTask = (token, idGroup, idTask, done) => ({
	type: "PROMISE",
	actions: ["CHANGE_DONE_TASK_LOADING", "CHANGE_DONE_TASK_LOADED", "CHANGE_DONE_TASK_FAILURE"],
	promise: api.changeDoneTask(token, idGroup, idTask, done)
})

export let _deleteGroup = (token, idGroup) => ({
	type: "PROMISE",
	actions: ["DELETE_GROUP_LOADING", "DELETE_GROUP_LOADED", "DELETE_GROUP_FAILURE"],
	promise: api.deleteGroup(token, idGroup)
})

export let _deleteTask = (token, idGroup, idTask) => ({
	type: "PROMISE",
	actions: ["DELETE_TASK_LOADING", "DELETE_TASK_LOADED", "DELETE_TASK_FAILURE"],
	promise: api.deleteTask(token, idGroup, idTask)
})