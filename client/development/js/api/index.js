import auth from "./auth";
import addTask from "./addTask";
import addGroup from "./addGroup";
import changeTask from "./changeTask";
import deleteTask from "./deleteTask";
import renameGroup from "./renameGroup";
import deleteGroup from "./deleteGroup";
import getUserState from "./getUserState";
import registration from "./registration";
import changeDoneTask from "./changeDoneTask";
import checkEmailForAbsence from "./checkEmailForAbsence";

export {
	auth,
	addTask,
	addGroup,
	deleteTask,
	changeTask,
	renameGroup,
	deleteGroup,
	getUserState,
	registration,
	changeDoneTask,
	checkEmailForAbsence
};
