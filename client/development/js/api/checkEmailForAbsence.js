import config from '../config';
import fetch from 'isomorphic-fetch';

export default function(email){
	return fetch(`${config.domainAPI}/checkEmailForAbsence/${email}`, {
		method: 'get',
		mode: 'cors',
		credentials: 'same-origin'
	}).then(res => {
		if(res.ok){
			return res.json();
		}{
			throw res;
		};
	});
}