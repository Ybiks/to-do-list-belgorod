import config from '../config';
import fetch from 'isomorphic-fetch';

export default function(token, data){
	return fetch(`${config.domainAPI}/userState`, {
		method: 'get',
		headers: {
			'Authorization': token
		},
		mode: 'cors',
		credentials: 'same-origin'
	}).then(res => {
		if(res.ok){
			return res.json();
		}{
			throw res;
		};
	});
}