import config from "../config";
import fetch from "isomorphic-fetch";

export default data => {
	return fetch(`${config.domainAPI}/auth/local`, {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		mode: 'cors',
		credentials: 'same-origin',
		body:  JSON.stringify(data),
	}).then(res => {
		if(res.ok){
			return res.json();
		}{
			throw res;
		};
	});
};