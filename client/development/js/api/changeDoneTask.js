import config from "../config";
import fetch from "isomorphic-fetch";

export default (token, idGroup, idTask, done) => {
	return fetch(`${config.domainAPI}/changeDoneTask`, {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json',
			'Authorization': token
		},
		mode: 'cors',
		credentials: 'same-origin',
		body:  JSON.stringify({idGroup, idTask, done}),
	}).then(res => {
		if(res.ok){
			return res.json();
		}{
			throw res;
		};
	});
};