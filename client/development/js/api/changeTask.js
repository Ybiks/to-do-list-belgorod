import config from "../config";
import fetch from "isomorphic-fetch";

export default (token, idGroup, idTask, description) => {
	return fetch(`${config.domainAPI}/changeTask`, {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json',
			'Authorization': token
		},
		mode: 'cors',
		credentials: 'same-origin',
		body:  JSON.stringify({idGroup, idTask, description}),
	}).then(res => {
		if(res.ok){
			return res.json();
		}{
			throw res;
		};
	});
};