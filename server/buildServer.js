import http from 'http'
import config from 'config'
import route from '../modules/route';


route().listen(process.env.PORT || config.get('port'), () => {
	console.log(`Server listen in ${process.env.PORT || config.get('port')} port`);
})