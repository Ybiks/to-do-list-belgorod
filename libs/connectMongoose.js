import config from 'config';
import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
mongoose.connect(config.get('mongoDB.host'));

export default mongoose;