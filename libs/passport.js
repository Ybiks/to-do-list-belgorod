import config from "config";
import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";

import { authorizeUser, authorizeUserById } from "../middleware/dataBase";

const jwtOptions = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: config.get("JwtSecret")
};

passport.use(
	new LocalStrategy(
		{
			usernameField: "email",
			session: false
		},
		function(username, password, done) {
			authorizeUser(username, password, function(err, user, info) {
				if (err) return done(null, false, info);

				if (!user) return done(null, false, info);

				return done(null, user, info);
			});
		}
	)
);

passport.use(
	new JwtStrategy(jwtOptions, function(payload, done) {
		authorizeUserById(payload.id, (err, user, info) => {
			if (err) return done(null, false, info);

			if (!user) return done(null, false, info);

			return done(null, user, info);
		});
	})
);

passport.serializeUser((user, done) => done(null, user.id));

passport.deserializeUser((id, done) =>
	dataBase.findUserById(id, (err, user) => done(err, user))
);

export default passport;
